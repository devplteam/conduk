$(function () {

    $(window).scroll(function(e){

        var scrollTop = window.pageYOffset ? window.pageYOffset : (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);

        var sH=$(window).height();
    });


    $(".big-slider").slick({
        dots: true,
        prevArrow: '<div class="slide-left"><i class="fa fa-angle-left fa-3x"></i></div>',
        nextArrow: '<div class="slide-right"><i class="fa fa-angle-right fa-3x"></i></div>'
    })

    $(".slider-partner").slick({
        prevArrow: '<div class="slide-left"><i class="fa fa-angle-left fa-2x"></i></div>',
        nextArrow: '<div class="slide-right"><i class="fa fa-angle-right fa-2x"></i></div>',
        slidesToShow: 3,
        slidesToScroll: 1
    });

    map = new GMaps({
        div: '#map',
        lat: -12.043333,
        lng: -77.028333,
        disableDefaultUI: true
    });

    map.addMarker({
        lat: -12.043333,
        lng: -77.028333,
        title: 'ул. Пушкинская 365',
        infoWindow: {
            content: '<div class="info-win"><div class="clearfix"><div class="col-md-7"><div class="line"><div class="title">Контактный телефон:</div><div class="box-phone">8 (800) 888-88-88</div><div class="title">Электронная почта:</div><div class="box-mail">info@climatcentr.com</div><div class="title">Сайт конференции:</div><div class="box-site"> www.climatcentr.com</div></div></div><div class="col-md-5 text-center"><div class="title-cheme">Схема проезда</div><img src="img/logo.png" alt="Logo"></div></div></div>'
        }
    });
});

var modal={
    'open': function (el) {
        $(el).fadeIn();
        $(".backdoor").fadeIn();
    },

    'close': function (el) {
        $(el).fadeOut();
        $(".backdoor").fadeOut();
    }
}